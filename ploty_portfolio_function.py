#! python3
# ploty_portfolio_function.py - 

import plotly
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.figure_factory as ff
import plotly.express as px


# function to create top 10 table
def top_10_table_info(tmp_fig, tmp_data, tmp_cols, sort_index):
    # create sorted table for top 10 account/agency
    for i, col in tmp_cols.items():
        tmp_top_data = tmp_data[col]
        tmp_top_data = tmp_top_data.sort_values(by=tmp_top_data.columns[sort_index], ascending=False).head(10)
        
        tmp_table = go.Table(header = dict(values=col),
                              cells = dict(values=[tmp_top_data[k].tolist() for k in tmp_top_data.columns[0:]]))
        
        tmp_fig.add_trace(tmp_table, row=1, col=i)


# Plot 1 - create plot for Demand Comparsion
def plotly_demand_comparsion_function(region_history_count, region_arrival_rn_revenue):
    fig1 = make_subplots(rows=3, cols=1, subplot_titles=('3 years comparsion on RNs by Created Year and Month', '3 years comparsion on RNs by Arrival Year and Month', 
                                                         '3 years comparsion on Total Revenue by Arrival Year and Month'),
                         column_widths=[1.0], row_heights=[1.0, 1.0, 1.0], shared_xaxes=True)
    
    # previous and next 3 years
    years = sorted([now.year - i for i in range(-3, 3)])
    
    # color index
    cols = plotly.colors.DEFAULT_PLOTLY_COLORS
    
    for i, year in enumerate(years):
        # 3 years comparsion on RN by Created Year and Month
        line1 = go.Scatter(x=region_history_count[region_history_count['Booked Year'] == int(year)]['Booked Month'], 
                           y=region_history_count[region_history_count['Booked Year'] == int(year)]['NumberOfBK'], 
                           mode='lines+markers', line={'color': cols[i]}, name=str(int(year)), legendgroup=str(year), showlegend=False)
        
        # 3 years comparsion on RNs by Arrival Year and Month
        line2 = go.Scatter(x=region_arrival_rn_revenue[region_arrival_rn_revenue['Arrival Year'] == int(year)]['Arrival Month'], 
                           y=region_arrival_rn_revenue[region_arrival_rn_revenue['Arrival Year'] == int(year)]['Blended Roomnights'], 
                           mode='lines+markers', line={'color': cols[i]}, name=str(int(year)), legendgroup=str(year))
        
        # 3 years comparsion on Total Revenue by Arrival Year and Month
        line3 = go.Scatter(x=region_arrival_rn_revenue[region_arrival_rn_revenue['Arrival Year'] == int(year)]['Arrival Month'], 
                           y=region_arrival_rn_revenue[region_arrival_rn_revenue['Arrival Year'] == int(year)]['Blended Guestroom Revenue Total'], 
                           mode='lines+markers', line={'color': cols[i]}, name=str(int(year)), legendgroup=str(year), showlegend=False)
        
        fig1.add_trace(line1, row=1, col=1)
        fig1.add_trace(line2, row=2, col=1)
        fig1.add_trace(line3, row=3, col=1)
    
    fig1.update_layout(title='3 years Demand History Comparsion', xaxis_title='Month', xaxis_showticklabels=True, height=1000)

    return fig1


# Plot 2 - create plot for Top Account
def plotly_top_account_function(region_top_account):
    fig2 = make_subplots(rows=1, cols=4, subplot_titles=('Top 10 Most Bookings', 'Top 10 Most RNs', 'Top 10 Most Total Revenue', 'Top 10 ADR'), 
                        column_widths=[0.05, 0.05, 0.05, 0.05], row_heights=[0.3], vertical_spacing=0.0, horizontal_spacing=0.005, 
                        specs=[[{"type": "table"}, {"type": "table"}, {"type": "table"}, {"type": "table"}]])
    
    top_10_ac_cols = {1 : ['Account', 'Industry', 'No. of Booking'], 2 : ['Account', 'Industry', 'RNs'], 3 : ['Account', 'Industry', 'Total Revenue'], 4 : ['Account', 'Industry', 'ADR']}
    top_10_table_info(fig2, region_top_account, top_10_ac_cols, 2)
    
    fig2.update_layout(title='Top 10 Account information', autosize=False, width=2000, height=800)

    return fig2


# Plot 3 - create plot for Top Account Industry
def plotly_top_acc_industry_function(region_account_ind, top_industry_list):
    fig3 = go.Figure()
    
    annotations = []
    cols = plotly.colors.DEFAULT_PLOTLY_COLORS
    
    # Top 10 account
    for industry in top_industry_list:
        tmp_top = (region_account_ind[region_account_ind['Account: Industry'] == industry].sort_values('size', ascending=False).head(7))
        tmp_top['percent'] = (tmp_top['size'] / tmp_top['size'].sum()).astype(float).map("{:.1%}".format)
        
        for index, (p, a, i) in enumerate(zip(tmp_top['percent'], tmp_top['Account: Industry'], tmp_top['Account: Province'])):
            fig3.add_trace(go.Bar(x=[p], y=[a], orientation='h', name=i, text=p, textposition='inside',
                                  hovertemplate="Province=%s<br>percent=%%{x}<br><extra></extra>" % i,
                                  marker=dict(color=cols[index], line=dict(color='rgb(248, 248, 249)', width=1))
                                  ))
            
            annotations.append(dict(xref='paper', yref='y',
                                x=0.14, y=a,
                                xanchor='right',
                                text=str(a),
                                font=dict(family='Arial', size=20,
                                          color='rgb(67, 67, 67)'),
                                showarrow=False, align='right'))
    
    
    fig3.update_layout(xaxis=dict(showgrid=False, showline=False, showticklabels=False, zeroline=False, domain=[0.15, 1]),
                       yaxis=dict(showgrid=False,showline=False,showticklabels=False,zeroline=False,),
                       barmode='stack',
                       paper_bgcolor='rgb(248, 248, 255)',
                       plot_bgcolor='rgb(248, 248, 255)',
                       margin=dict(l=120, r=10, t=140, b=80),
                       showlegend=False)
    
    fig3.update_layout(title="Target Industry's Top 7 Province", annotations=annotations)
    
    return fig3


# Plot 4 - create plot for Top Agency
def plotly_top_agency_function(region_top_agency):
    fig4 = make_subplots(rows=1, cols=4, subplot_titles=('Top 10 Most Bookings', 'Top 10 Most RNs', 'Top 10 Most Total Revenue', 'Top 10 ADR'), 
                        column_widths=[0.05, 0.05, 0.05, 0.05], row_heights=[0.3], vertical_spacing=0.0, horizontal_spacing=0.005, 
                        specs=[[{"type": "table"}, {"type": "table"}, {"type": "table"}, {"type": "table"}]])
    
    top_10_ag_cols = {1 : ['Agency', 'No. of Booking'], 2 : ['Agency', 'RNs'], 3 : ['Agency', 'Total Revenue'], 4 : ['Agency', 'ADR']}
    top_10_table_info(fig4, region_top_agency, top_10_ag_cols, 1)
    
    fig4.update_layout(title='Top 10 Agency information', autosize=False, width=2000, height=800)
    
    return fig4


# Plot 5 - create plot for Top Agency Inustry
def plotly_top_ag_industry_function(region_top_ag_industry, top_agency_list):
    fig5 = go.Figure()
    
    annotations = []
    cols = plotly.colors.DEFAULT_PLOTLY_COLORS
    
    # Top 10 account
    for acc in top_agency_list:
        tmp_top = (region_top_ag_industry[region_top_ag_industry['Agency Id'] == acc].sort_values('size', ascending=False).head(5))
        tmp_top['percent'] = (tmp_top['size'] / tmp_top['size'].sum()).astype(float).map("{:.1%}".format)
        
        for index, (p, a, i) in enumerate(zip(tmp_top['percent'], tmp_top['Agency'], tmp_top['End User SIC'])):
            fig5.add_trace(go.Bar(x=[p], y=[a], orientation='h', name=i, text=p, textposition='inside',
                                  hovertemplate="Industry=%s<br>percent=%%{x}<br><extra></extra>" % i,
                                  marker=dict(color=cols[index], line=dict(color='rgb(248, 248, 249)', width=1))
                                  ))
            
            annotations.append(dict(xref='paper', yref='y',
                                x=0.14, y=a,
                                xanchor='right',
                                text=str(a),
                                font=dict(family='Arial', size=10,
                                          color='rgb(67, 67, 67)'),
                                showarrow=False, align='right'))
    
    
    fig5.update_layout(xaxis=dict(showgrid=False, showline=False, showticklabels=False, zeroline=False, domain=[0.15, 1]),
                       yaxis=dict(showgrid=False,showline=False,showticklabels=False,zeroline=False,),
                       barmode='stack',
                       paper_bgcolor='rgb(248, 248, 255)',
                       plot_bgcolor='rgb(248, 248, 255)',
                       margin=dict(l=120, r=10, t=140, b=80),
                       showlegend=False)
    
    fig5.update_layout(title="Top 5 Agency Booking Industry", annotations=annotations)
    
    return fig5


# Plot 6 - create plot for created vs arrival
def plotly_created_vs_arrival_function(region_created_vs_arrival):
    fig6 = px.sunburst(region_created_vs_arrival, path=['Created Month', 'month_bins'], values='size')
    
    fig6.update_layout(title='Relationship between Booking Created and Arrival Month', autosize=False, width=1800, height=800)

    return fig6


# Plot 7 & 8 - create plot for RFGM score
def plotly_rfgm_function(rfgm_score_all):
    
    
    def plotly_table_figure(fig_tmp, dataframe, columns):
        global plot_number
        dataframe = dataframe[columns]
        table_tmp = go.Table(header = dict(values=columns),
                             cells = dict(values=[dataframe[k].tolist() for k in dataframe.columns[0:]]))
        fig_tmp.add_trace(table_tmp, row=plot_number, col=1)
        plot_number += 1
        
        
    # function to create rfm analysis table
    rfgm_display_col = ['Account', 'Account: Region', 'Account: Industry', 'Recency', 'Frequency', 'Guestroom_value', 'Monetary_value']
    
    
    
    fig7 = make_subplots(rows=3, cols=1, subplot_titles=('Loyal Customers', 'Big Spenders on Guestroom', 'Big Spenders on Total Revenue'), 
                        column_widths=[0.05], row_heights=[0.3, 0.3, 0.3], vertical_spacing=0.1, horizontal_spacing=0.0, 
                        specs=[[{"type": "table"}], [{"type": "table"}], [{"type": "table"}]])
    
    fig8 = make_subplots(rows=3, cols=1, subplot_titles=('Promising Customers', 'New Potential Customers', 'Gone Big Spenders'), 
                        column_widths=[0.05], row_heights=[0.3, 0.3, 0.3], vertical_spacing=0.1, horizontal_spacing=0.0, 
                        specs=[[{"type": "table"}], [{"type": "table"}], [{"type": "table"}]])
    
    # Reset plot_number
    plot_number = 1
    plt_7_loyal_customer = rfgm_score_all[rfgm_score_all['F'] >= 3].sort_values('Frequency', ascending=False)
    plotly_table_figure(fig7, plt_7_loyal_customer, rfgm_display_col)
    
    plt_7_big_spender_RN = rfgm_score_all[rfgm_score_all['G'] >= 3].sort_values('Guestroom_value', ascending=False)
    plotly_table_figure(fig7, plt_7_big_spender_RN, rfgm_display_col)
    
    plt_7_big_spender_Rev = rfgm_score_all[(rfgm_score_all['M'] >= 3) & (rfgm_score_all['R'] <= 3)].sort_values('Monetary_value', ascending=False)
    plotly_table_figure(fig7, plt_7_big_spender_Rev, rfgm_display_col)
    
    # Reset plot_number
    plot_number = 1
    plt_8_promising_customer = rfgm_score_all[(rfgm_score_all['F'] == 2) & (rfgm_score_all['M'] <= 3) & (rfgm_score_all['R'] <= 3)].sort_values('Frequency', ascending=False)
    plotly_table_figure(fig8, plt_8_promising_customer, rfgm_display_col)
    
    plt_8_new_potential = rfgm_score_all[(rfgm_score_all['R'] == 5) & (rfgm_score_all['Frequency'] <= 2)].sort_values('Monetary_value', ascending=False)
    plotly_table_figure(fig8, plt_8_new_potential, rfgm_display_col)
    
    plt_8_gone_big_spender = rfgm_score_all[(rfgm_score_all['R'] >= 2) & (rfgm_score_all['M'] >= 2)].sort_values('Monetary_value', ascending=False)
    plotly_table_figure(fig8, plt_8_gone_big_spender, rfgm_display_col)
    
    fig7.update_layout(title='Customer RFGM scores (as of 2021/12/31)', autosize=False, width=1500, height=800)
    
    fig8.update_layout(autosize=False, width=1500, height=800)

    return fig7, fig8


# combine all plot into one html and save as
def figures_to_html(figs, filename):
    dashboard = open(filename, 'w')
    dashboard.write("<html><head></head><body>" + "\n")
    for fig in figs:
        inner_html = fig.to_html().split('<body>')[1].split('</body>')[0]
        dashboard.write(inner_html)
    dashboard.write("</body></html>" + "\n")


# main function ploty_portfolio_function
def ploty_portfolio_function(region, region_history_count, region_arrival_rn_revenue, region_top_account, region_account_ind, region_top_agency, region_top_ag_industry, top_industry_list, top_agency_list, region_created_vs_arrival, rfgm_score_all):
    
    fig1 = plotly_demand_comparsion_function(region_history_count, region_arrival_rn_revenue)
    
    fig2 = plotly_top_account_function(region_top_account)
    
    fig3 = plotly_top_acc_industry_function(region_account_ind, top_industry_list)
    
    fig4 = plotly_top_agency_function(region_top_agency)
    
    fig5 = plotly_top_ag_industry_function(region_top_ag_industry, top_agency_list)
    
    fig6 = plotly_created_vs_arrival_function(region_created_vs_arrival)
    
    fig7, fig8 = plotly_rfgm_function(rfgm_score_all)
    
    filename = region +'_portfolio_dashboard.html'
    figures_to_html([fig1, fig2, fig3, fig4, fig5, fig6, fig7, fig8], filename=filename)
    
    
