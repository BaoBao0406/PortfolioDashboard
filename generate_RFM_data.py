#! python3
# generate_RFM_data.py - 

import datetime
import pandas as pd
import numpy as np



# create RFGM table
def rfgm_df(BK_tmp):
    RFGM_df = BK_tmp.groupby(['Account Id', 'Account']).agg({'Arrival_row': lambda x: (NOW - x.max()).days,
                                                             'Arrival_china': lambda x: (NOW - x.max()).days,
                                                             'Account Id': lambda x: len(x), 
                                                             'Blended Roomnights': lambda x: x.sum(),
                                                             'Blended Total Revenue': lambda x: x.sum()})
    RFGM_df.rename(columns={'Arrival_row': 'Recency row', 
                            'Arrival_china': 'Recency china', 
                            'Account Id': 'Frequency',
                            'Blended Roomnights': 'Guestroom_value',
                            'Blended Total Revenue': 'Monetary_value'}, inplace=True)
    return RFGM_df


# calculate RFGM quatiles scores
def rfgm_score_quat(RFGM_tmp, rfgm_dict):
    for key, values in rfgm_dict.items():
        tmp_labels = values[0]
        tmp_quartiles = pd.cut(RFGM_tmp[key], 4, labels=tmp_labels)
        RFGM_tmp[values[1]] = tmp_quartiles.values
    
    return RFGM_tmp


# perform RFM data preprocess
def preprocess_RFM_data(data):
    
    BK_tmp = data[['Booking ID#', 'ArrivalDate', 'Account Id', 'Account', 'Account: Region', 'Account: Industry', 'Agency Id', 'Agency', 'Booking Type', 'Blended Roomnights', 'Blended Total Revenue', 'Account Type']]
    
    # Noted that the current date is set to 2022-01-01
    NOW = datetime.datetime(2022,1,1)
    BK_tmp['ArrivalDate'] = pd.to_datetime(BK_tmp['ArrivalDate'])
    BK_tmp = BK_tmp[BK_tmp['ArrivalDate'] < NOW]
    
    BK_tmp['year'] = pd.DatetimeIndex(BK_tmp['ArrivalDate']).year
    BK_tmp['month'] = pd.DatetimeIndex(BK_tmp['ArrivalDate']).month
    BK_tmp['day'] = pd.DatetimeIndex(BK_tmp['ArrivalDate']).day
    
    BK_tmp['Arrival_china'] = pd.to_datetime(BK_tmp[['year', 'month', 'day']], errors='coerce')
    BK_tmp['Arrival_china'] = np.where(BK_tmp['Account: Region'].isin(['China', 'Macau']), BK_tmp['Arrival_china'], np.datetime64('NaT'))
    # For all bookings except China and Macau, noted that due to COVID-19, for arrival year before 2020, the year will increase 2 years to match china and macau market for 
    # calculation purpose of Recency.
    
    BK_tmp['year'] = BK_tmp['year'].apply(lambda x: x + 2 if x < 2020 else x)
    BK_tmp['Arrival_row'] = pd.to_datetime(BK_tmp[['year', 'month', 'day']], errors='coerce')
    BK_tmp['Arrival_row'] = np.where(~BK_tmp['Account: Region'].isin(['China', 'Macau']), BK_tmp['Arrival_row'], np.datetime64('NaT'))
    
    # Take out Account 'ROTARY CLUB OF MACAU'
    acct_name = 'ROTARY CLUB OF MACAU'
    BK_tmp = BK_tmp[BK_tmp['Account'] != acct_name]
    
    return BK_tmp
    

# generate RFM socre and fix the outlier problem
def generate_RFM_score_n_outlier(data, rfgm_dict):
    RFGM_df = rfgm_df(data)
    
    RFGM_score = rfgm_score_quat(RFGM_df, rfgm_dict)
    
    # Take out the outlier data for late process (combine it later)
    RFGM_df = pd.DataFrame(RFGM_df).reset_index()
    acct_name = ['JEUNESSE GLOBAL HOLDINGS, LLC TAIWAN BR']
    outliers = RFGM_df.loc[RFGM_df['Account'].isin(acct_name)]
    outliers_row = data[data['Account'].isin(acct_name)].index
    BK_cv_wo_outliner_tmp = data.drop(outliers_row)
    
    RFGM_df_no_outlier = rfgm_df(BK_cv_wo_outliner_tmp)
    
    RFGM_score_no_outlier = pd.DataFrame(rfgm_score_quat(RFGM_df_no_outlier, rfgm_dict)).reset_index()
    RFGM_score_all = pd.concat([RFGM_score_no_outlier, outliers], ignore_index=True)
    
    region_sic = data[['Account Id', 'Account: Region', 'Account: Industry']].drop_duplicates(subset=['Account Id'])
    RFGM_score_all = pd.merge(RFGM_score_all, region_sic, on='Account Id', how='left')
    
    RFGM_score_all['Recency'] = np.where(RFGM_score_all['Account: Region'].isin(['China', 'Macau']), RFGM_score_all['Recency china'], RFGM_score_all['Recency row'])
    RFGM_score_all['R'] = np.where(RFGM_score_all['Account: Region'].isin(['China', 'Macau']), RFGM_score_all['R china'], RFGM_score_all['R row'])
    
    return RFGM_score_all


# main function
def generate_RFM_data(data):
    
    BK_tmp = preprocess_RFM_data(data)
    
    rfgm_dict = {'Recency china': [range(4, 0, -1), 'R china'], 'Recency row': [range(4, 0, -1), 'R row'], 
                 'Frequency': [range(1, 5), 'F'], 'Guestroom_value': [range(1, 5), 'G'], 'Monetary_value': [range(1, 5), 'M']}
    
    RFGM_score_all = generate_RFM_score_n_outlier(BK_tmp, rfgm_dict)
    
    
    return RFGM_score_all
