#! python3
# extract_sql_portfolio_data.py - 

import pyodbc
import datetime
import pandas as pd
from dateutil.relativedelta import relativedelta



def sql_user_data(conn):
    # FDC User ID and Name list
    user = pd.read_sql('SELECT DISTINCT(Id), Name \
                        FROM dbo.[User]', conn)
    user = user.set_index('Id')['Name'].to_dict()
    
    return user


#
def sql_booked_data(conn, user, start_date, end_date):
    booked_df = pd.read_sql("SELECT BK.OwnerId, BK.nihrm__Property__c, ac.Name, ac.BillingCountry, ag.Name, BK.Name, FORMAT(BK.nihrm__ArrivalDate__c, 'MM/dd/yyyy') AS ArrivalDate, BK.VCL_Arrival_Year__c, BK.VCL_Arrival_Month__c, FORMAT(BK.nihrm__DepartureDate__c, 'MM/dd/yyyy') AS DepartureDate, \
                                     BK.nihrm__CurrentBlendedRoomnightsTotal__c, BK.nihrm__BlendedGuestroomRevenueTotal__c, \
                                     BK.VCL_Blended_F_B_Revenue__c, BK.nihrm__CurrentBlendedEventRevenue7__c, BK.nihrm__BookingStatus__c, FORMAT(BK.nihrm__LastStatusDate__c, 'MM/dd/yyyy') AS LastStatusDate, \
                                     FORMAT(BK.nihrm__BookedDate__c, 'MM/dd/yyyy') AS BookedDate, BK.Booked_Year__c, BK.Booked_Month__c, BK.End_User_Region__c, BK.End_User_SIC__c, BK.nihrm__BookingTypeName__c, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__DateDefinite__c, 'MM/dd/yyyy') AS DateDefinite, BK.Date_Definite_Month__c, BK.Date_Definite_Year__c \
                              FROM dbo.nihrm__Booking__c AS BK \
                              LEFT JOIN dbo.Account AS ac \
                                  ON BK.nihrm__Account__c = ac.Id \
                              LEFT JOIN dbo.Account AS ag \
                                  ON BK.nihrm__Agency__c = ag.Id \
                              WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal')) AND \
                                  (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND \
                                  (BK.nihrm__BookedDate__c BETWEEN CONVERT(datetime, '" + start_date + "') AND CONVERT(datetime, '" + end_date + "'))", conn)
    booked_df.columns = ['Owner Name', 'Property', 'Account', 'Company Country', 'Agency', 'Booking: Booking Post As', 'Arrival', 'Arrival Year', 'Arrival Month', 'Departure', 
                          'Blended Roomnights', 'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue', 'Status',
                          'Last Status Date', 'Booked', 'Booked Year', 'Booked Month', 'End User Region', 'End User SIC', 'Booking Type', 'Booking ID#', 'DateDefinite', 'Date Definite Month', 'Date Definite Year']
    booked_df['Owner Name'].replace(user, inplace=True)

    return booked_df


#
def sql_arrival_data(conn, user, start_date, end_date):
    arrival_df = pd.read_sql("SELECT BK.OwnerId, BK.nihrm__Property__c, ac.Name, ac.BillingCountry, ag.Name, BK.Name, FORMAT(BK.nihrm__ArrivalDate__c, 'MM/dd/yyyy') AS ArrivalDate, BK.VCL_Arrival_Year__c, BK.VCL_Arrival_Month__c, FORMAT(BK.nihrm__DepartureDate__c, 'MM/dd/yyyy') AS DepartureDate, \
                                        BK.nihrm__CurrentBlendedRoomnightsTotal__c, BK.nihrm__BlendedGuestroomRevenueTotal__c, \
                                        BK.VCL_Blended_F_B_Revenue__c, BK.nihrm__CurrentBlendedEventRevenue7__c, BK.nihrm__BookingStatus__c, FORMAT(BK.nihrm__LastStatusDate__c, 'MM/dd/yyyy') AS LastStatusDate, \
                                        FORMAT(BK.nihrm__BookedDate__c, 'MM/dd/yyyy') AS BookedDate, BK.Booked_Year__c, BK.Booked_Month__c, BK.End_User_Region__c, BK.End_User_SIC__c, BK.nihrm__BookingTypeName__c, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__DateDefinite__c, 'MM/dd/yyyy') AS DateDefinite, BK.Date_Definite_Month__c, BK.Date_Definite_Year__c \
                                    FROM dbo.nihrm__Booking__c AS BK \
                                    LEFT JOIN dbo.Account AS ac \
                                        ON BK.nihrm__Account__c = ac.Id \
                                    LEFT JOIN dbo.Account AS ag \
                                        ON BK.nihrm__Agency__c = ag.Id \
                                    WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal')) AND \
                                        (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND \
                                        (BK.nihrm__ArrivalDate__c BETWEEN CONVERT(datetime, '" + start_date + "') AND CONVERT(datetime, '" + end_date + "'))", conn)
    arrival_df.columns = ['Owner Name', 'Property', 'Account', 'Company Country', 'Agency', 'Booking: Booking Post As', 'Arrival', 'Arrival Year', 'Arrival Month', 'Departure', 
                                'Blended Roomnights', 'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue', 'Status',
                                'Last Status Date', 'Booked', 'Booked Year', 'Booked Month', 'End User Region', 'End User SIC', 'Booking Type', 'Booking ID#', 'DateDefinite', 'Date Definite Month', 'Date Definite Year']
    arrival_df['Owner Name'].replace(user, inplace=True)
    arrival_df['Days before Arrive'] = ((pd.to_datetime(arrival_df['Arrival']) - pd.Timestamp.now().normalize()).dt.days).astype(int)
    arrival_df.sort_values(by=['Days before Arrive'], inplace=True)

    return arrival_df


# query All booking data
def sql_all_data(conn):
    BK_all_df = pd.read_sql("SELECT BK.Id, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__ArrivalDate__c, 'yyyy-MM-dd') AS ArrivalDate, ac.Id, ac.Name AS ACName, ag.Id, ag.Name AS AGName, BK.End_User_Region__c, BK.End_User_SIC__c, \
                                 BK.nihrm__BookingTypeName__c, ac.nihrm__RegionName__c, ac.Industry, ac.BillingState, ag.nihrm__RegionName__c, ag.BillingState, BK.nihrm__CurrentBlendedRoomnightsTotal__c, BK.nihrm__CurrentBlendedRevenueTotal__c, \
                                 BK.nihrm__CurrentBlendedADR__c, FORMAT(BK.nihrm__BookedDate__c, 'yyyy-MM-dd') AS BookedDate, BK.nihrm__Property__c, ac.Type, ag.Type \
                          FROM dbo.nihrm__Booking__c AS BK \
                                 LEFT JOIN dbo.Account AS ac \
                                     ON BK.nihrm__Account__c = ac.Id \
                                 LEFT JOIN dbo.Account AS ag \
                                     ON BK.nihrm__Agency__c = ag.Id \
                          WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal', 'CS Catering - Social')) AND \
                                 (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND (BK.nihrm__BookingStatus__c IN ('Definite'))", conn)
    BK_all_df.columns = ['Id', 'Booking ID#', 'ArrivalDate', 'Account Id', 'Account', 'Agency Id', 'Agency', 'End User Region', 'End User SIC', 'Booking Type', 'Account: Region', 'Account: Industry', 'Account: Province', 'Agency: Region',
                      'Agency: Province', 'Blended Roomnights', 'Blended Total Revenue', 'Blended ADR', 'BookedDate', 'Property', 'Account Type', 'Agency Type']

    return BK_all_df


def extract_sql_portfolio_data():
    
    # Set date range
    now = datetime.datetime.now()
    start_year = now - relativedelta(years=3)
    start_date = str(start_year.year) + '-01-01'
    # Booked date data
    end_date_booked = str(now.year) + '-12-31'
    # Arrival date data
    end_year_arrival = now + relativedelta(years=5)
    end_date_arrival = str(end_year_arrival.year) + '-12-31'
    
    
    conn = pyodbc.connect('Driver={SQL Server};'
                          'Server=VOPPSCLDBN01\VOPPSCLDBI01;'
                          'Database=SalesForce;'
                          'Trusted_Connection=yes;')

    user = sql_user_data(conn)
    
    booked_df = sql_booked_data(conn, user, start_date, end_date_booked)
    
    arrival_df = sql_arrival_data(conn, user, start_date, end_date_arrival)
    
    BK_all_df = sql_all_data(conn)
    
    return booked_df, arrival_df, BK_all_df
