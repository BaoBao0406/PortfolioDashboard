#! python3
# preprocess_filtering_portfolio_data.py - 

import pandas as pd
import numpy as np



# Plot 1 - Pre process demand comparsion data
def preprocess_demand_comparsion(region_booked_df, region_arrival_df):
    # default month name
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    
    region_history_count = region_booked_df.groupby(['Booked Month', 'Booked Year']).size().reset_index(name='NumberOfBK')
    region_history_count['to_sort']=region_history_count['Booked Month'].apply(lambda x: months.index(x))
    region_history_count = region_history_count.sort_values('to_sort')
    
    region_arrival_rn_revenue = region_arrival_df.groupby(['Arrival Month', 'Arrival Year']).sum().reset_index()
    region_arrival_rn_revenue['to_sort'] = region_arrival_rn_revenue['Arrival Month'].apply(lambda x: months.index(x))
    region_arrival_rn_revenue = region_arrival_rn_revenue.sort_values('to_sort')

    return region_history_count, region_arrival_rn_revenue


# Plot 2 - Pre process Top Account data
def preprocess_top_account(region_account_BK):
    region_top_account = region_account_BK[region_account_BK['Account Type'] == 'Account']
    region_top_account = region_top_account.groupby(['Account Id', 'Account', 'Account: Industry']).agg({'Account Id': lambda x: len(x), 
                                                                                                       'Blended Roomnights': lambda x: x.sum(),
                                                                                                       'Blended Total Revenue': lambda x: x.sum(),
                                                                                                       'Blended ADR' : lambda x: x.mean()})
    region_top_account.rename(columns={'Account Id': 'No. of Booking', 'Blended Roomnights': 'RNs', 'Blended Total Revenue': 'Total Revenue', 'Blended ADR': 'ADR'}, inplace=True)
    region_top_account = pd.DataFrame(region_top_account).reset_index()
    region_top_account.rename(columns={'Account: Industry': 'Industry'}, inplace=True)

    return region_top_account


# Plot 3 - Pre process Top Account Industry data
def preprocess_top_acc_industry(region_account_BK):
    #TODO function to get top industry
    top_industry_list = ['Pharmaceutical & Drug', 'Healthcare']
    
    region_account_ind = region_account_BK[region_account_BK['Account Type'] == 'Account']
    region_account_ind = region_account_ind[region_account_ind['Account: Industry'].isin(top_industry_list)]
    region_account_ind = region_account_ind.groupby(['Account: Industry', 'Account: Province']).size().to_frame('size')
    region_account_ind = pd.DataFrame(region_account_ind).reset_index()

    return region_account_ind, top_industry_list


# Plot 4 - Pre process Top Agency data
def preprocess_top_agency(region_agency_BK):
    region_top_agency = region_agency_BK[region_agency_BK['Agency Type'] == 'Agency']
    region_top_agency = region_top_agency.groupby(['Agency Id', 'Agency']).agg({'Agency Id': lambda x: len(x), 
                                                                              'Blended Roomnights': lambda x: x.sum(),
                                                                              'Blended Total Revenue': lambda x: x.sum(),
                                                                              'Blended ADR': lambda x: x.mean()})
    region_top_agency.rename(columns={'Agency Id': 'No. of Booking', 'Blended Roomnights': 'RNs', 'Blended Total Revenue': 'Total Revenue', 'Blended ADR': 'ADR'}, inplace=True)
    region_top_agency = pd.DataFrame(region_top_agency).reset_index()

    return region_top_agency


# Plot 4 - Pre process Top Agency Industry data
def preprocess_top_ag_industry(region_agency_BK):
    region_top_ag_industry = region_agency_BK[region_agency_BK['Agency Type'] == 'Agency']
    top_agency_list = region_top_ag_industry.sort_values(by=region_top_ag_industry.columns[2], ascending=False).head(5)['Agency Id'].to_list()
    region_top_ag_industry = region_top_ag_industry[region_top_ag_industry['Agency Id'].isin(top_agency_list)][['Agency Id', 'Agency', 'End User SIC']]
    region_top_ag_industry = region_top_ag_industry.groupby(['Agency Id', 'Agency', 'End User SIC']).size().to_frame('size')
    region_top_ag_industry = pd.DataFrame(region_top_ag_industry).reset_index()

    return region_top_ag_industry, top_agency_list


# Pre process data - Plot 6
def preprocess_created_vs_arrival(region_booking_BK):
    # create label and bins for chart
    bins, labels = [], []
    
    for i in range(37):
        labels.append(str((i+1)*30) + ' days')
        bins.append(i*30)
    bins.append(np.inf)
    
    region_created_vs_arrival = region_booking_BK[['BookedDate', 'ArrivalDate']]
    region_created_vs_arrival['ArrivalDate'] = pd.to_datetime(region_created_vs_arrival['ArrivalDate'])
    region_created_vs_arrival['BookedDate'] = pd.to_datetime(region_created_vs_arrival['BookedDate'])
    region_created_vs_arrival['month_diff'] = (region_created_vs_arrival['ArrivalDate'] - region_created_vs_arrival['BookedDate']).dt.days
    region_created_vs_arrival['month_bins'] = pd.cut(region_created_vs_arrival['month_diff'], bins, labels=labels)
    region_created_vs_arrival['Created Month'] = region_created_vs_arrival['BookedDate'].dt.month_name().str[:3]
    #created_vs_arrival['Arrival Month'] = created_vs_arrival['ArrivalDate'].dt.month_name().str[:3]
    region_created_vs_arrival = region_created_vs_arrival.groupby(['Created Month', 'month_bins']).size().to_frame('size')
    region_created_vs_arrival = pd.DataFrame(region_created_vs_arrival).reset_index()

    return region_created_vs_arrival


# Filter by industry or region
def filtering_region_data(region, booked_df, arrival_df, BK_all_df):
    # plot 1
    region_booked_df = booked_df[booked_df['End User Region'] == region]
    region_arrival_df = arrival_df[arrival_df['End User Region'] == region]
    
    # plot 2 & 3
    region_account_BK = BK_all_df[BK_all_df['Account: Region'] == region]
    region_agency_BK = BK_all_df[BK_all_df['Agency: Region'] == region]
    
    # plot 4 & 5
    region_booking_BK = BK_all_df[BK_all_df['End User Region'] == region]
    
    return region_booked_df, region_arrival_df, region_account_BK, region_agency_BK, region_booking_BK


# main function
def preprocess_filtering_portfolio_data(region, booked_df, arrival_df, BK_all_df, RFGM_score_all):
    
    region_booked_df, region_arrival_df, region_account_BK, region_agency_BK, region_booking_BK = filtering_region_data(region, booked_df, arrival_df, BK_all_df)
    
    # Plot 1
    region_history_count, region_arrival_rn_revenue = preprocess_demand_comparsion(region_booked_df, region_arrival_df)
    # Plot 2
    region_top_account = preprocess_top_account(region_account_BK)
    # Plot 3
    region_account_ind, top_industry_list = preprocess_top_acc_industry(region_account_BK)
    # Plot 4
    region_top_agency = preprocess_top_agency(region_agency_BK)
    # Plot 5
    region_top_ag_industry, top_agency_list = preprocess_top_ag_industry(region_agency_BK)
    # Plot 6
    region_created_vs_arrival = preprocess_created_vs_arrival(region_booking_BK)
    
    return region_history_count, region_arrival_rn_revenue, region_top_account, region_account_ind, region_top_agency, region_top_ag_industry, top_industry_list, top_agency_list, region_created_vs_arrival

