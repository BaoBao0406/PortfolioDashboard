#! python3
# portfolio_dashboard_main.py - 

import pandas as pd
import numpy as np
import extract_sql_portfolio_data, generate_RFM_data, preprocess_filtering_portfolio_data, ploty_portfolio_function



# main function portfolio_dashboard_main
def portfolio_dashboard_main():
    
    booked_df, arrival_df, BK_all_df = extract_sql_portfolio_data()
    
    rfgm_score_all = generate_RFM_data(BK_all_df)
    
    region_list = ['Asia Pacific', 'Australia/New Zealand', 'Canada', 'Caribbean', 'China', 'Europe', 'Hong Kong', 'India', 'Indonesia', 'Japan'
              'Korea', 'Macau', 'Malaysia', 'Middle East/Africa', 'Russia', 'Singapore', 'South America', 'Taiwan', 'Thailand', 'USA']
    
    # loop for each region in list
    for region in region_list:
        
        region_history_count, region_arrival_rn_revenue, region_top_account, region_account_ind, region_top_agency, region_top_ag_industry, top_industry_list, top_agency_list, region_created_vs_arrival = preprocess_filtering_portfolio_data(region, booked_df, arrival_df, BK_all_df, rfgm_score_all)
    
        ploty_portfolio_function(region, region_history_count, region_arrival_rn_revenue, region_top_account, region_account_ind, region_top_agency, region_top_ag_industry, top_industry_list, top_agency_list, region_created_vs_arrival, rfgm_score_all)
        
        
